const router = require('express').Router();
const inventory = require('../data/inventory.json')
const warehouses = require('../data/warehouses.json')

//router.get inventory
router.get('/inventory', (req, res) => {
    res.json(inventory);
})

//router.get product details
router.get('/inventory/:id', (req, res) => {
    let target = String(req.params.id);
    res.json(inventory.find(each => each.id === target));
})

//router.get locations
router.get('/locations', (req, res) => {
    res.json(warehouses);
})

//router.get location details
router.get('/locations/:id', (req, res) => {
    let target = String(req.params.id);
    res.json(warehouses.find(each => each.id === target));
})

//router.get inventory with specific id
router.get('/whInventory/:id', (req, res) => {
    let target = String(req.params.id);
    res.json(inventory.filter(each => each.warehouseId === target));
})

//router.post add new product
router.post('/newProduct', (req, res) => {
    // retrieve new product object from the POST body
    let newProduct = req.body;

    if (req.body) {
        //create new ID
        let id = "I" + String((Date.now()) + (Math.random().toString(36).slice(2)));

        //grab each value from the post body
        let newname = newProduct.name;
        let newlastOrdered = newProduct.lastOrdered;

        //create location value
        let newcity = newProduct.city;
        let newcountry = newProduct.country;
        let newlocation = newcity + " ," + newcountry;

        let newquantity = newProduct.quantity;
        let newisInstock = newProduct.isInstock;
        let newdescription = newProduct.description;

        //grab a warehouse to link product to
        let categories = warehouses[0].categories;
        let warehouseId = warehouses[0];

        //create product item
        let newProductItem = {
            "id": id,
            "name": newname,
            "description": newdescription,
            "quantity": newquantity,
            "lastOrdered": newlastOrdered,
            "location": newlocation,
            "isInstock": newisInstock,
            "categories": categories,
            "warehouseId": warehouseId
        }

        //push new product item as json into inventory
        inventory.push(newProductItem);

        //respond with inventory item
        res.json(inventory);
    }

    else {
        res.status(400).send('Please complete fields in form');
    }
})

//router.post add new warehouse
router.post('/newLocation', (req, res) => {
    // retrieve new product object from the POST body
    let newLocation = req.body;
    
    if (req.body) {
        //create new ID
        let id = "W" + String((Date.now()) + (Math.random().toString(36).slice(2)));

        //grab each value from the post body
        let name = newLocation.name;

        // let address = newLocation.address;

        let street = newLocation.street;
        let suiteNum = newLocation.suiteNum;
        let postal = newLocation.postal;

        let city = newLocation.city;
        let province = newLocation.province;

        let cName = newLocation.contactName;
        let cTitle = newLocation.position;
        let cPhone = newLocation.phone;
        let cEmail = newLocation.email;

        let inventoryCategories = newLocation.categories

        //create product item
        let newWarehouse = {
            "id": id,
            "name": name,
            "address": {
                "street": street,
                "suiteNum": suiteNum,
                "city": city,
                "province": province,
                "postal": postal
            },
            "contact": {
                "name": cName,
                "title": cTitle,
                "phone": cPhone,
                "email": cEmail
            },
            "inventoryCategories": inventoryCategories
        }

        //push new product item as json into inventory
        warehouses.push(newWarehouse);

        //respond with inventory item
        res.json(warehouses);
    }

    else {
        res.status(400).send('Please complete fields in form');
    }
})
//delete product endpoint
router.delete('/deleteProd/:id', (req, res) => {

    let idToDelete = req.body.id;
    let indexToDelete = inventory.findIndex(index => index.id === idToDelete)
    //delete entry by index number
    inventory.splice(indexToDelete, 1);
    //respond with array with deleted index
    res.json(inventory);
})

module.exports = router;