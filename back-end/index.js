const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const PORT = 8080;
let app = express();
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors({
    origin:"http://localhost:3000"
}))


//put all routes in the routes file
app.use('/inStock', require('./routes/InStockRoutes'));

app.listen(PORT, () =>{
    console.log("Listening on port: " + PORT)
})