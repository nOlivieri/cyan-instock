import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import './styles/navbar.scss'
import image from './Logo-instock.svg'

class Navbar extends Component {
  constructor() {
    super();
    this.state = {
      invActive: false,
      locActive: false
    }
  }

  invClassToggle = (e) => {
    this.setState({ invActive: true, locActive: false })
  }

  locClassToggle = (e) => {
    this.setState({ locActive: true, invActive: false })
  }

  render() {
    return (
      <div className="navbar">
        <Link to={"/inventory"} style={{ textDecoration: 'none' }}>
          <img src={image} alt="instockLogo" className="navbar__logo" />
        </Link>
        <div className="navbar__link">
          <Link to={"/inventory"} style={{ textDecoration: 'none' }}>
            <div onClick={this.invClassToggle}
              className={this.state.invActive ? "active" : "navbar__link__inventory"} >
              <h2>Inventory</h2>
            </div>
          </Link>
          <Link to={"/locations"} style={{ textDecoration: 'none' }}>
            <div onClick={this.locClassToggle}
              className={this.state.locActive ? "active" : "navbar__link__locations"}>
              <h2>Locations</h2>
            </div>
          </Link>
        </div>
      </div>
    )
  }
}

export default Navbar;