import React, { Component } from 'react'
import './styles/warehouse.scss'

export default class WarehouseProduct extends Component {

  render() {
    if (!this.props.product) {
      return 'loading...'
    } else {
      return (
        <tr>
          <th className="warehouseProduct__hiddenColumn"><h5>ITEM</h5></th>
          <td><h5>{this.props.product.name}</h5>
            <p>{this.props.product.description}</p>
          </td>
          <th className="warehouseProduct__hiddenColumn"><h5>LAST ORDERED</h5></th>
          <td>{this.props.product.lastOrdered}</td>
          <th className="warehouseProduct__hiddenColumn"><h5>LOCATION</h5></th>
          <td>{this.props.product.location}</td>
          <th className="warehouseProduct__hiddenColumn"><h5>QUANTITY</h5></th>
          <td>{this.props.product.quantity}</td>
          <th className="warehouseProduct__hiddenColumn"><h5>STATUS</h5></th>
          <td className="warehouse__status-button">{this.props.product.isInstock} <img src="/Assets/Icons/SVG/Icon-kebab-default.svg" alt="icon" /></td>
        </tr>
      )
    }
  }
}
