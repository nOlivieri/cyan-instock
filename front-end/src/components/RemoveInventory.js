import React, { Component } from 'react';
import './styles/inventory.scss';
import axios from 'axios';

export default class RemoveInventory extends React.Component{

    constructor(){
        super();
    }
    
    removeInventory = (e) => {
        e.preventDefault();
        axios.delete(`http://localhost:8080/instock/deleteProd/${this.props.id}`, {data:{id:this.props.id}})
        .then(res => {
            this.props.addInventoryData(res.data);
        })
        .catch((err) => {
            console.log(err);
        })
    }
    
    render(){
        return(
        <div className="inventory-delete__menu">
            <a className="delete-link" href="" onClick={this.removeInventory} >Remove</a> 
        </div>
        )
    }
}