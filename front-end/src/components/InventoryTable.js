import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect, Link } from 'react-router-dom';
import './styles/inventory.scss';
import RemoveInventory from './RemoveInventory';

export default class InventoryTable extends React.Component {
    constructor() {
        super();
        this.state = {
            removeBtnShowing: false
        };
    }
    showRemoveBtn = () => {
        const { removeBtnShowing } = this.state;
        this.setState({
            removeBtnShowing: !removeBtnShowing
        });
    }

    hideRemoveBtn = () => {
        if (this.state.removeBtnShowing) {
            this.setState({ removeBtnShowing: false })
        }
    }

    render() {
        let instocktext = "";

        if (this.props.isInstock === "false") {
            instocktext = "Out of Stock"
        }
        else {
            instocktext = "In Stock"
        }
        
        return (

            <tr className="inventory-table__component" onClick={this.hideRemoveBtn}>
                <th className="inventory-table__hidden-column">
                    <h5>ITEM</h5>
                </th>
                <td>
                    <Link to={`/inventory/${this.props.id}`}>
                        <h5 className="inventory-table__title">{this.props.name}</h5>
                    </Link>
                    <p>{this.props.description}</p>
                </td>
                <th className="inventory-table__hidden-column">
                    <h5>LAST ORDERED</h5>
                </th>
                <td><p>{this.props.lastOrdered}</p></td>
                <th className="inventory-table__hidden-column">
                    <h5>LOCATION</h5>
                </th>
                <td><p>{this.props.location}</p></td>
                <th className="inventory-table__hidden-column">
                    <h5>QUANTITY</h5>
                </th>
                <td><p>{this.props.quantity}</p></td>
                <th className="inventory-table__hidden-column">
                    <h5>STATUS</h5>
                </th>
                <td className="inventory-table__status">
                    <div className="inventory-delete" onClick={this.showRemoveBtn}>
                        <p> {instocktext} </p>
                        <button className="inventory-table__delete-button">
                            {this.state.removeBtnShowing && <RemoveInventory
                                removeInventory={this.props.removeInventory}
                                id={this.props.id}
                                addInventoryData={this.props.addInventoryData}
                            />}
                            <img src="Assets/Icons/SVG/Icon-kebab-default.svg" alt="" />
                        </button>
                    </div>
                </td>
            </tr>
        )
    }
}
