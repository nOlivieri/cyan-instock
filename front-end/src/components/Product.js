import React, { Component } from 'react';
import './styles/product.scss';
import axios from 'axios';
import { Link } from 'react-router-dom';

export default class Product extends Component {

    componentDidMount() {
        const id = this.props.match.params.id;

        axios.get(`http://localhost:8080/instock/inventory/${id}`)
            .then((res) => {
                this.props.addInventoryItem(res.data);
            })
            .catch((err) => {
                console.log(err);
            })
    }

    render() {

        let { name, isInstock, description, lastOrdered, location, quantity, categories, id, key } = this.props.inventoryItem;

        let instockclass = "";
        let instocktext = ""

        if (isInstock === "false") {
            instockclass = "outofstock";
            instocktext = "Out of Stock"
        }
        else {
            instockclass = "instock";
            instocktext = "In Stock"
        }

        return (

            <div className="product">
                <div className="product__header">
                    <div className="product__header__title">
                        <Link to="/inventory">
                            <img src="../../Assets/Icons/SVG/Icon-back-arrow.svg" alt="back arrow" className="mobileHidden unposition" />
                        </Link>
                        <h1>{name}</h1>
                    </div>
                    <div className={instockclass}>
                        {instocktext}
                    </div>
                </div>
                <hr className="mobileHidden line"></hr>
                <div className="product__info">
                    <div className="product__info__description">
                        <h5 className="label">ITEM DESCRIPTION</h5>
                        <p className="text">{description}</p>
                    </div>
                    <div className="product__info__details">
                        <div className="row">
                            <div className="order">
                                <div className="orderedBy">
                                    <h5 className="label">ORDERED BY</h5>
                                    <p className="text">{name}</p>
                                </div>
                                <div className="lastOrdered">
                                    <h5 className="label">LAST ORDERED</h5>
                                    <p className="text">{lastOrdered}</p>
                                </div>
                            </div>
                            <div className="refLocation">
                                <div className="reference">
                                    <h5 className="label">REFERENCE NUMBER</h5>
                                    <p className="text">{id}</p>
                                </div>
                                <div className="location">
                                    <h5 className="label">LOCATION</h5>
                                    <p className="text">{location}</p>
                                </div>
                            </div>
                        </div>
                        <div className="quantity mobileHidden unposition">
                            <h5 className="label">QUANTITY</h5>
                            <p className="text">{quantity}</p>
                        </div>
                        <div className="categories">
                            <h5 className="label">CATEGORIES</h5>
                            <p className="text lastpara">{categories}</p>
                        </div>
                    </div>
                </div>
                <hr className="mobileHidden line"></hr>
                <button>EDIT</button>
            </div>
        )
    }
}
