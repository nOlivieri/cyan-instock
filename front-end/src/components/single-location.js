import React, { Component } from 'react'
import './styles/locations.scss'
import { Link } from 'react-router-dom';

class SingleLocation extends Component {
  render() {
    return (
      <>
        <tr className="wareName-and-address-tablet">
          <strong>{this.props.wareName}</strong><br />{this.props.address.street}
        </tr>
        <tr className="single-location-row" id={this.props.id} key={this.props.id}>
          <td className="wareName-and-address"><strong>{this.props.wareName}</strong><br />{this.props.address.street}</td>
          <td className="name-and-title">{this.props.contact.name}<br /><i>{this.props.contact.title}</i></td>
          <td className="phone-and-email">{this.props.contact.phone}<br />{this.props.contact.email}</td>
          <td className="single-location__categories">{this.props.inventoryCategories}</td>
          <td>
            <Link to={`/locations/${this.props.id}`} className="linkArrow">
              <img src="Assets/Icons/SVG/Icon-arrow-right.svg" alt="" />
            </Link>
          </td>
        </tr>
      </>

    )
  }
}

export default SingleLocation;