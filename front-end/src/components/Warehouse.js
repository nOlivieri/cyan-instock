import React, { Component } from 'react'
import './styles/warehouse.scss'
import WarehouseProduct from './WarehouseProduct';
import { Link } from 'react-router-dom';
import axios from 'axios';

export default class Warehouse extends Component {

  componentDidMount() {
    const itemId = this.props.match.params.id;

    axios.get(`http://localhost:8080/inStock/locations/${itemId}`)
      .then((res) => {
        this.props.addLocationItem(res.data);
      })
      .catch((err) => {
        console.log(err);
      })
    axios.get(`http://localhost:8080/inStock/whInventory/${itemId}`)
      .then((res) => {
        this.props.addInventoryItem(res.data);
      })
      .catch((err) => {
        console.log(err);
      })
  }

  render() {
    let { name, key } = this.props.locationItem;
    let address = Object(this.props.locationItem.address);
    let contact = Object(this.props.locationItem.contact);

    return (
      <div className="mainTable">
        <div className="tableDiv">
          <div className="tableDiv__headerIcon">
            <Link to={"/locations"} style={{ alignSelf: 'center' }}><img className="tableDiv__Icon" src="/Assets/Icons/SVG/Icon-back-arrow.svg" alt="icon"></img></Link>
            <h2 className="tableDiv__header">{name}</h2>
          </div>
          <table className="tableDiv__table">
            <thead className="tableDiv__thead">
              <tr className="warehouseTable__hiddenRow">
                <th className="tableDiv--Setsize"><h5>ADDRESS</h5></th>
                <th className="tableDiv--Setsize"><h5>CONTACT</h5></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th className="warehouseProduct__hiddenColumn"><h5>ADDRESS</h5></th>
                <td>{address.street + ' ' + address.suiteNum}</td>
                <td>{contact.name + ' ' + contact.title}</td>
              </tr>
              <tr>
                <th className="warehouseProduct__hiddenColumn"><h5>CONTACT</h5></th>
                <td>{address.city + ', ' + address.province + ' ' + address.postal + ' CA'}</td>
                <td>{contact.phone + ' ' + contact.email}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="warehouse">
          <table className="warehouseTable">
            <tbody>
              <tr className="warehouseTable__hiddenRow">
                <th className="sizing"><h5>ITEM</h5></th>
                <th><h5>LAST ORDERED</h5></th>
                <th><h5>LOCATION</h5></th>
                <th><h5>QUANTITY</h5></th>
                <th><h5>STATUS</h5></th>
              </tr>
              <WarehouseProduct product={this.props.inventory[0]} />
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}
