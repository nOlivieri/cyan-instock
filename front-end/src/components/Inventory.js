import React, { Component } from 'react';
import './styles/inventory.scss';
import axios from 'axios';
import InventoryTable from './InventoryTable';

export default class Inventory extends Component {

    componentDidMount() {
        axios.get("http://localhost:8080/instock/inventory")
            .then((res) => {
                this.props.addInventoryData(res.data);
            })
            .catch((err) => {
                console.log(err);
            })
    }

    render() {
        let showAddnew = this.props.showAddnew;
        let closeAddnew = this.props.closeAddew;
        let inventoryMap = this.props.inventory.map((object) => {
            return <InventoryTable id={object.id}
                name={object.name}
                description={object.description}
                lastOrdered={object.lastOrdered}
                location={object.location}
                quantity={object.quantity}
                isInstock={object.isInstock}
                key={object.id}
                categories={object.categories}
                showAddnew={showAddnew}
                closeAddnew={closeAddnew}
                addInventoryData={this.props.addInventoryData} />
        })
        
        return (
            <div className="inventory">
                <div className="inventory-header">
                    <h1>Inventory</h1>
                    <div className="inventory-header__search-bar">
                        <img className="inventory-header__search-bar--img" src="Assets/Icons/SVG/Icon-search.svg" alt="" />
                        <input className="inventory-header__search-bar--input" type="text" placeholder="Search" name="search" />
                    </div>
                </div>
                <table className="inventory-table">
                    <tbody>
                        <tr className="inventory-table__hidden-row">
                            <th><h5>ITEM</h5></th>
                            <th><h5>LAST ORDERED</h5></th>
                            <th><h5>LOCATION</h5></th>
                            <th><h5>QUANTITY</h5></th>
                            <th><h5>STATUS</h5></th>
                        </tr>
                        {inventoryMap}
                    </tbody>
                </table>
                <div onClick={this.props.showAddnew} className="add-button">
                    <img id="add-icon" src="Assets/Icons/SVG/Icon-add.svg" alt="add-icon" />
                </div>
            </div>
        )
    }
}

