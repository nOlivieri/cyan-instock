import React, { Component } from 'react'
import './styles/addnew.scss'
import Switch from "react-switch";
import axios from 'axios';

class Addnew extends Component {
    constructor() {
        super();
        this.state = { checked: false };
    }

    handleChange = (checked) => {
        this.setState({ checked });
    }

    addInvent = (e) => {
        let newName = e.target.name.value;
        let newLastOrdered = e.target.lastOrdered.value;
        let newCity = e.target.city.value;
        let newCountry = e.target.country.value;
        let newQuantity = e.target.quantity.value;
        let newInStock = e.target.inStock.value;
        let newDescription = e.target.description.value;

        const newItemObj = {
            name: newName,
            lastOrdered: newLastOrdered,
            city: newCity,
            country: newCountry,
            quantity: newQuantity,
            isInStock: newInStock,
            description: newDescription
        }

        var post = {
            method: 'POST',
            url: 'http://localhost:8080/instock/newProduct',
            data: newItemObj,
            headers: {
                'content-type': 'application/json'
            }
        }
        axios(post)
            .then((result) => {
                console.log(result.data);
            })
            .catch((err) => {
                console.log(err);
            })
    }

    render() {
        return (
            <div className="bg--modal">
                <div className="modal">
                    <div className="modal__title">
                        <h1>Create New</h1>
                    </div>
                    <form onSubmit={this.addInvent}>
                        <div className="modal__flex">
                            <div className="modal__flex__item">
                                <h5>PRODUCT</h5>
                                <input name="name" type="text" placeholder="Item Name" className="modal__flex__item__input" />
                            </div>
                            <div className="modal__flex__item">
                                <h5>LAST ORDERED</h5>
                                <input name="lastOrdered" type="text" placeholder="yyyy-mm-dd" className="modal__flex__item__input" />
                            </div>
                            <div className="modal__flex__item">
                                <h5>CITY</h5>
                                <input name="city" type="text" placeholder="City" className="modal__flex__item__input" />
                            </div>
                            <div className="modal__flex__item">
                                <h5>COUNTRY</h5>
                                <input name="country" type="text" placeholder="Canada" className="modal__flex__item__input" />
                            </div>
                            <div className="modal__flex__item" id="qtymodal">
                                <h5>QUANTITY</h5>
                                <input name="quantity" type="number" placeholder="0" className="modal__flex__item__input" />
                            </div>
                            <div className="modal__flex__item" id="switchDiv">
                                <h5>STATUS</h5>
                                <label htmlFor="instock-switch" className="modal__flex__item--flex">
                                    <span>In Stock</span>
                                    <Switch
                                        name="inStock"
                                        onColor="#00cc00"
                                        onHandleColor="#ffffff"
                                        handleDiameter={30}
                                        uncheckedIcon={false}
                                        checkedIcon={false}
                                        boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                                        activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                                        height={30}
                                        width={52}
                                        className="modal__flex__item__switch"
                                        id="switch"
                                        checked={this.state.checked}
                                        onChange={this.handleChange}
                                    />
                                </label>
                            </div>
                            <div className="modal__flex__item">
                                <h5>ITEM DESCRIPTION</h5>
                                <input name="description" type="text" placeholder="(Optional)" className="modal__flex__item__input--textarea" />
                            </div>
                        </div>
                        <div className="modal__btn">
                            <button className="modal__btn__Sbtn">
                                <p>SAVE</p>
                            </button>
                            <button onClick={this.props.closeAddnew} className="modal__btn__Cbtn">
                                <p>CANCEL</p>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}
export default Addnew;