import React, { Component } from 'react'
import SingleLocation from './single-location'
import './styles/locations.scss'
import axios from 'axios';

class Locations extends Component {

  componentDidMount() {
    axios.get("http://localhost:8080/instock/locations")
      .then((res) => {
        this.props.addLocationData(res.data);
      })
      .catch((err) => {
        console.log(err);
      })
  }

  render() {

    let singleLocationMap = this.props.warehouses.map((object, index) => {
      return <SingleLocation id={this.props.warehouses[index].id}
        wareName={this.props.warehouses[index].name}
        address={this.props.warehouses[index].address}
        contact={this.props.warehouses[index].contact}
        inventoryCategories={this.props.warehouses[index].inventoryCategories} />
    })

    return (
      <section className="location-list">
        <div className="locations-header">
          <h1 className="locations-header__title">Locations</h1>
          <input className="locations-header__input" type="text" placeholder="Search" />
        </div>

        <table className="locations_table">
          <thead className="tableHeadings">
            <th className="tableHeadings__warehouse">WAREHOUSE</th>
            <th className="tableHeadings__contact">CONTACT</th>
            <th className="tableHeadings__info">CONTACT INFORMATION</th>
            <th className="tableHeadings__category">CATEGORIES</th>
          </thead>
          {singleLocationMap}
        </table>
        <div onClick={this.props.showAddWare} className="add-button">
          <img id="add-icon" src="Assets/Icons/SVG/Icon-add.svg" alt="add-icon" />
        </div>
      </section>
    )
  }
}

export default Locations;