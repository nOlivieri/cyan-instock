import React, { Component } from 'react'
import './styles/addnewModal.scss'
import axios from 'axios';

class Addnew extends Component {

    addWare = (e) => {
        let newName = e.target.name.value;
        let newStreet = e.target.street.value;
        let newsuiteNum = e.target.suiteNum.value;
        let newPostal = e.target.postal.value;
        let newCity = e.target.city.value;
        let newProvince = e.target.province.value;
        let newContactName = e.target.contactName.value;
        let newPosition = e.target.position.value;
        let newPhone = e.target.phone.value;
        let newEmail = e.target.email.value;
        let newInventoryCategories = e.target.inventoryCategories.value;

        const newItemObj = {
            name: newName,
            street: newStreet,
            suiteNum: newsuiteNum,
            postal: newPostal,
            city: newCity,
            province: newProvince,
            contactName: newContactName,
            position: newPosition,
            phone: newPhone,
            email: newEmail,
            inventoryCategories: newInventoryCategories
        }

        var post = {
            method: 'POST',
            url: 'http://localhost:8080/instock/newLocation',
            data: newItemObj,
            headers: {
                'content-type': 'application/json'
            }
        }
        axios(post)
            .then((result) => {
                console.log(result.data);
            })
            .catch((err) => {
                console.log(err);
            })
    }

    render() {
        return (
            <div className="bg--modal">
                <div className="modal2">
                    <div className="modal2__title">
                        <h1>Create New</h1>
                    </div>
                    <form onSubmit={this.addWare}>
                        <div className="modal2__flex">
                            <div className="modal2__flex__item">
                                <h5>WAREHOUSE</h5>
                                <input name="name" type="text" placeholder="Warehouse Name" className="modal2__flex__item__input" />
                            </div>
                            <div className="modal2__flex__item">
                                <h5>STREET</h5>
                                <input name="street" type="text" placeholder="Enter Street" className="modal2__flex__item__input" />
                            </div>
                            <div className="modal2__flex__item">
                                <h5>SUITE #</h5>
                                <input name="suiteNum" type="text" placeholder="Enter Suite" className="modal2__flex__item__input" />
                            </div>
                            <div className="modal2__flex__item">
                                <h5>ZIP/POSTAL CODE</h5>
                                <input name="postal" type="text" placeholder="ex.A5A 5A5" className="modal2__flex__item__input" />
                            </div>
                            <div className="modal2__flex__item">
                                <h5>CITY</h5>
                                <input name="city" type="text" placeholder="Enter City" className="modal2__flex__item__input" />
                            </div>
                            <div className="modal2__flex__item">
                                <h5>PROVINCE</h5>
                                <input name="province" type="text" placeholder="Enter Province" className="modal2__flex__item__input" />
                            </div>
                            <div className="modal2__flex__item">
                                <h5>CONTACT NAME</h5>
                                <input name="contactName" type="text" placeholder="Enter Name" className="modal2__flex__item__input" />
                            </div>
                            <div className="modal2__flex__item">
                                <h5>POSITION</h5>
                                <input name="position" type="text" placeholder="Enter Position" className="modal2__flex__item__input" />
                            </div>
                            <div className="modal2__flex__item">
                                <h5>PHONE #</h5>
                                <input name="phone" type="tel" placeholder="(000) 000 - 0000" className="modal2__flex__item__input" />
                            </div>
                            <div className="modal2__flex__item">
                                <h5>EMAIL</h5>
                                <input name="email" type="email" placeholder="email@instock.com" className="modal2__flex__item__input" />
                            </div>
                            <div className="modal2__flex__item">
                                <h5>ITEM DESCRIPTION</h5>
                                <input name="inventoryCategories" type="text" placeholder="ex. Power-Tool, Electric" className="modal2__flex__item__input--textarea" />
                            </div>
                        </div>
                        <div className="modal2__btn">
                            <button className="modal2__btn__Sbtn">
                                <p>SAVE</p>
                            </button>
                            <button onClick={this.props.closeAddWare} className="modal2__btn__Cbtn">
                                <p>CANCEL</p>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default Addnew;