import React, { Component } from 'react';
import './App.css';
import './fonts/stylesheet.css';
import Navbar from './components/Navbar'
import Locations from './components/locations'
import Product from './components/Product'
import Inventory from './components/Inventory';
import Warehouse from './components/Warehouse';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Addnew from './components/Addnew';
import AddnewModal from './components/AddnewModal';

class App extends Component {
  state = {
    inventory: [],
    warehouses: [],
    showAddnew: false,
    showAddWare: false,
    inventoryItem: {},
    locationItem: {}
  }

  //Toggle Form-Addnew Instanciation
  showAddnew = (e) => {
    this.setState({ showAddnew: true })
  }

  showAddWare = (e) => {
    this.setState({ showAddWare: true })
  }

  closeAddWare = (e) => {
    this.setState({ showAddWare: false })
  }

  closeAddnew = (e) => {
    this.setState({ showAddnew: false })
  }

  addLocationData = (locationsArray) => {
    this.setState({
      warehouses: locationsArray
    })
  }

  addInventoryData = (inventoryArray) => {
    this.setState({
      inventory: inventoryArray
    })
  }

  addInventoryItem = (item) => {
    this.setState({
      inventoryItem: item
    })
  }

  addLocationItem = (item) => {
    this.setState({
      locationItem: item
    })
  }

  render() {
    //Conditional rendering for Form-Addnew
    let addnew = this.state.showAddnew
    let addnewWindow;
    if (addnew) {
      addnewWindow = <Addnew closeAddnew={this.closeAddnew} />
    }
    let showAddWare = this.state.showAddWare
    let addnewWare;
    if (showAddWare) {
      addnewWare = <AddnewModal closeAddWare={this.closeAddWare} />
    }

    return (
      <Router>
        <div className="app">
          <Navbar />
          {addnewWindow}
          {addnewWare}
          <Switch>
            <Redirect path={"/"} to={"/inventory"} exact component={Inventory} />
            <Route path="/inventory" exact render={(props) => {
              return (
                <Inventory {...props} inventory={this.state.inventory}
                  warehouses={this.state.warehouses}
                  addInventoryData={this.addInventoryData}
                  showAddnew={this.showAddnew}
                  closeAddnew={this.closeAddnew} />
              )
            }} />
            <Route path="/inventory/:id" render={(props) => {
              return (
                <Product {...props} inventoryItem={this.state.inventoryItem}
                  warehouses={this.state.warehouses}
                  addInventoryItem={this.addInventoryItem}
                />
              )
            }} />
            <Route path="/locations" exact render={(props) => {
              return (
                <Locations {...props} inventory={this.state.inventory}
                  warehouses={this.state.warehouses}
                  addLocationData={this.addLocationData}
                  showAddWare={this.showAddWare} />
              )
            }} />
            <Route path="/locations/:id" render={(props) => {
              return (
                <Warehouse {...props} inventory={this.state.inventoryItem}
                  warehouses={this.state.warehouses}
                  addLocationItem={this.addLocationItem}
                  locationItem={this.state.locationItem}
                  addInventoryItem={this.addInventoryItem} />
              )
            }} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
