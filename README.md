##DESCRIPTION:

Project Overview
Having worked on a few projects thus far, you are now ready to take on more complex work. Your task is to work with an assigned team to develop an inventory management system for a Fortune 500 client.

The client�s existing system is slow, unresponsive, and difficult to scale causing employees to have to use Excel. Clearly, it�s time for an upgrade.

Although you and your team will be building the product from the ground-up, your project manager has already set up a JIRA board with the required tasks outlined. All that�s left is to begin assigning tickets and developing your product.


## steps to install
1. front end - npm install
2. back end - npm install

## to start development servers
3. front-end 'npm start'
4. back-end 'nodemon index.js' or 'node index.js'
